# Contributor: Newbyte <newbie13xd@gmail.com>
# Contributor: Nulo <alpine@nulo.in>
# Contributor: psykose <alice@ayaya.dev>
# Maintainer: Nulo <alpine@nulo.in>
pkgname=tg_owt
pkgver=20220201
pkgrel=0
_commit="25c8637f5975db830cc5d74477641a8b609e248d"
_libyuv_commit="804980bbab748fd0e180cd6e7d9292ff49baf704"
pkgdesc="Telegram Desktop's fork of Google's WebRTC"
url="https://github.com/desktop-app/tg_owt"

# s390x: due to alleged issues on Big Endian platforms
# ppc64le: undefined sse2 optimizations
arch="all !ppc64le !s390x"

license="BSD-3-Clause"

# Sorted according to
# https://github.com/telegramdesktop/tdesktop/wiki/The-Packaged-Building-Mode
# abseil bundled because of linking issues
# https://github.com/desktop-app/tg_owt/pull/55#discussion_r599718405
# openh264 bundled because packaged is in testing
# TODO: usrsctp bundled because I wasn't able to figure out how to make cmake
# detect the packaged one.
# Not specified in the wiki page (see
# https://github.com/desktop-app/tg_owt/pull/55):
# pffft bundled because there's no stable ABI and patched
# rnnoise bundled because "all remaining files are custom"
# libsrtp bundled because tg_owt uses private APIs
# libyuv bundled because there's no stable ABI and has many breaking updates
depends_dev="
	libdrm-dev
	libepoxy-dev
	ffmpeg-dev
	mesa-dev
	glib-dev
	jpeg-dev
	openssl1.1-compat-dev
	opus-dev
	pipewire-dev
	libvpx-dev
	libx11-dev
	libxcomposite-dev
	libxdamage-dev
	libxext-dev
	libxfixes-dev
	libxrender-dev
	libxrandr-dev
	libxtst-dev
	"
makedepends="
	$depends_dev
	cmake
	yasm
	samurai
	"

subpackages="$pkgname-dev"
source="
	$pkgname-$_commit.tar.gz::https://github.com/desktop-app/tg_owt/archive/$_commit.tar.gz
	libyuv-$_libyuv_commit.zip::https://codeload.github.com/catdevnull/libyuv/zip/$_libyuv_commit
	"
builddir="$srcdir/$pkgname-$_commit"

prepare() {
	default_prepare
	mv ../libyuv-$_libyuv_commit/* src/third_party/libyuv
}

build() {
	# dynamic version has broken linking
	cmake -B build -G Ninja . \
		-DTG_OWT_PACKAGED_BUILD=True \
		-DBUILD_SHARED_LIBS=OFF \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=MinSizeRel

	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d3ac4c9a43fe401fc050a218c2d6626b39907326b256e85e532120ba469308de62522763fd1fcab1283c83bf9036be06dacce249e9dd4529017fcba6d39bce41  tg_owt-25c8637f5975db830cc5d74477641a8b609e248d.tar.gz
d433995219aaa3470cbccca93a02401c2f0c9c7badbe7d947902d8f4bcedd121548fca28e34a7dad6c6688f43382acbd00dff16b265bf7a66585d3da7e3a3285  libyuv-804980bbab748fd0e180cd6e7d9292ff49baf704.zip
"
