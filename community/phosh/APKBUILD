# Contributor: Bart Ribbers <bribbers@disroot.org>
# Contributor: Danct12 <danct12@disroot.org>
# Contributor: Newbyte <newbyte@disroot.org>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=phosh
pkgver=0.15.0
pkgrel=0
_commit_gvc="ae1a34aafce7026b8c0f65a43c9192d756fe1057"
_commit_libcall_ui="5c79ccfc017db217d3a23a211919604083438846"
pkgdesc="Shell PoC for the Librem5"
# Blocked on mips and s390x by gnome-session, gnome-settings-daemon, squeekboard and libhandy
# Blocked on ppc64le by gnome-session
# riscv64 disabled due to missing rust in recursive dependency
arch="all !s390x !ppc64le !mips !mips64 !riscv64"
url="https://gitlab.gnome.org/World/Phosh/phosh"
license="GPL-3.0-only"
depends="phoc gnome-session bash dbus-x11 gnome-settings-daemon
	squeekboard libpulse dbus:org.freedesktop.Secrets gnome-control-center"
makedepends="gtk+3.0-dev meson ninja gnome-desktop-dev callaudiod-dev libgudev-dev libhandy1-dev gcr-dev upower-dev
	linux-pam-dev git cmake pulseaudio-dev networkmanager-dev polkit-elogind-dev
	libsecret-dev feedbackd-dev elogind-dev wayland-protocols"
subpackages="$pkgname-dbg $pkgname-lang"
source="$pkgname-$pkgver.tar.gz::$url/-/archive/v$pkgver/phosh-v$pkgver.tar.gz
	https://gitlab.gnome.org/GNOME/libgnome-volume-control/-/archive/$_commit_gvc/gvc-$_commit_gvc.tar.gz
	https://gitlab.gnome.org/World/Phosh/libcall-ui/-/archive/$_commit_libcall_ui/libcall-ui-$_commit_libcall_ui.tar.gz
	phosh.desktop
	sm.puri.OSK0.desktop
	"
options="!check" # Needs a running Wayland compositor
builddir="$srcdir/$pkgname-v$pkgver"

prepare() {
	default_prepare
	subproject_path="$builddir"/subprojects
	gvc_path="$subproject_path"/gvc
	cui_path="$subproject_path"/libcall-ui
	mkdir -p $gvc_path $cui_path
	mv "$srcdir"/libgnome-volume-control-"$_commit_gvc"/* $gvc_path
	mv "$srcdir"/libcall-ui-"$_commit_libcall_ui"/* $cui_path
}

build() {
	abuild-meson . output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir/" meson install --no-rebuild -C output

	install -D -m644 "$srcdir"/phosh.desktop \
		"$pkgdir"/usr/share/wayland-sessions/phosh.desktop

	install -D -m644 "$srcdir"/sm.puri.OSK0.desktop \
		"$pkgdir"/usr/share/applications/sm.puri.OSK0.desktop

}
sha512sums="
bd560cb7446e27a2d4f8c59de93a27b63157cc151547cb224035f9d4b01275ca1c96dc341842cec0f213b332a63fb130c173af4e1ad990e6b42813eda41d3e4b  phosh-0.15.0.tar.gz
723334bff55927363dab47ef22c71dcaf94263fe76e49c40f1cbfbd5f86383e68fd4bf2182eb5777dda8e2ede4ee4710e1a7ab1379d3ca40d68f68ff30c62e21  gvc-ae1a34aafce7026b8c0f65a43c9192d756fe1057.tar.gz
e9c22efe724c3cfa9cc233f2abdea65c4ed6d366c9fd36c7865dbd7afb458f360ba7533dbb9898d705344f61f67b9232546466f322f29d508c12239ee288a488  libcall-ui-5c79ccfc017db217d3a23a211919604083438846.tar.gz
aef856033ae17bc8e18963ea56aad34e6e0d262d0060f9b5aa9f072c7598d531ee9a55b189d6fadf7e6f3c5dd113b77de51a64bcf17ad5764ff57a4be8472dc7  phosh.desktop
52a670893bd5027d6a45d7ff18258d634dc5d98371a7cdc094261ccc12a15d0b46e96d572ae8882ae27eb90418f2dc4f0fb1a45ce95f8d70eadc51d284780916  sm.puri.OSK0.desktop
"
